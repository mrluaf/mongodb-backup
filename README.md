# How to use
## Step 1. Clone this script to any end on VPS
```sh
git clone https://gitlab.com/mrluaf/mongodb-backup.git
cd mongodb-backup
```

## Step 2. Change the parameters

- Change in the mongo_backup.sh file to suit your system
- Just change 2 information fields: `MONGO_URI`, `DATABASE_NAME`
- See more infomation about MONGO_URI by [click here](https://www.mongodb.com/docs/manual/reference/connection-string)

```sh
# Example: mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]
MONGO_URI='mongodb://127.0.0.1:27017'

# Enter the name of the database to export data
DATABASE_NAME='tel'
```

## Step 3. Make the script executable.
```sh
sudo chmod +x mongo_backup.sh
```

## Step 4. You can run the backup script as shown.
```sh
./mongo_backup.sh
```

## Step 5. Save the output
- The system will automatically create 2 folders. You only need to care about the `.tar.gz` files in the folder `db_output`
- The file name is set in the following format: `YYYYMMDDHHmmss_DATABASE_NAME.tar.gz` (in the format of the [momentjs](https://momentjs.com/) library)
- You can save it by date or download the whole thing through the folder `db_output`


# FAQ
## Missing `mongodump`

Plase check [this document](https://www.mongodb.com/docs/database-tools/installation/installation/) for install it