export PATH=/bin:/usr/bin:/usr/local/bin
CURRENT_PATH=$PWD
TODAY=`date +"%Y%m%d%H%M%S"`
mkdir -p ./db_backup
mkdir -p ./db_output
DB_BACKUP_PATH='./db_backup'
# You don't need to worry about the above. Those are just default settings

# See more in: https://www.mongodb.com/docs/manual/reference/connection-string/
# Example: mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]
MONGO_URI='mongodb://127.0.0.1:27017'

# Enter the name of the database to export data
DATABASE_NAME='tel'

mkdir -p ${DB_BACKUP_PATH}/${TODAY}
mongodump --uri="${MONGO_URI}" -d ${DATABASE_NAME} --out ${DB_BACKUP_PATH}/${TODAY}/
cd ${CURRENT_PATH}/${DB_BACKUP_PATH}/${TODAY}/
pwd
echo ${CURRENT_PATH}/db_output/${TODAY}_${DATABASE_NAME}.tar.gz
tar -czf ${CURRENT_PATH}/db_output/${TODAY}_${DATABASE_NAME}.tar.gz ./${DATABASE_NAME}
cd $CURRENT_PATH

echo 'DB backup done!'